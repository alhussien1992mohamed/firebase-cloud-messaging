﻿1-  must create a project in firebase for notification (from https://firebase.google.com/products/cloud-messaging)
	and get (server key , sender ID )

2- install package from nuget

3-  IN appsetting.json add:
	 "Firebase": {
     "Server": "",
     "Sender": "762063350973",
     "Icon": ""
      },

4- in startup.cs file set this code :
	services.AddScoped<IMessages>(c ⇒ ActivatorUtilities.CreateInstance<Messages>(c,
	Configuration.GetValue<string>("Firebase:Server"),
	Configuration.GetValue<string>("Firebase:Sender"),
	Configuration.GetValue<string>("Firebase:Icon")));

5- setup client listiner for notification from as link :
	- for java : https://firebase.google.com/docs/cloud-messaging/js/client
	- for android : https://firebase.google.com/docs/cloud-messaging/android/client
	- for ios :https://firebase.google.com/docs/cloud-messaging/ios/client