﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace FirebaseMessages
{
    public interface IMessages
    {
        string Send(Message message);
       
        string Send(string token, string title, string message, string Type = "Notify", string link = null, string ObjectID = null,Dictionary<string, string> data = null);
        //Task<string[]> Send(string[] tokens, string title, string message, string Type = "Notify", string link = null, string ObjectID = null);
    }
}