﻿


using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace FirebaseMessages
{
    public class Messages : IMessages
    {
        private string Server { get; set; }
        private string Sender { get; set; }
        private string IconUrl { get; set; }
        
        
        /// <param name="Server">from firebase account</param>
        /// <param name="Sender">from firebase account</param>
        /// <param name="iconUrl">Must be online image </param>
        public Messages(string Server, string Sender, string iconUrl)
        {
            this.Server = Server; this.Sender = Sender; this.IconUrl = iconUrl;
        }
        public string Send(Message message)
        {
            return Send( message.to , message.data.title, message.data.body, message.data.type, message.data.click_action, message.data.objectID,data:message.Data);
        }
        public string Send(string token, string title, string message, string Type = "Notify", string link = null, string ObjectID = null,Dictionary<string, string> data=null)
        {
            string sResponseFromServer = "";
            try
            {
                WebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");
                tRequest.Method = "post";
                //serverKey - Key from Firebase cloud messaging server  
                tRequest.Headers.Add(string.Format("Authorization: key={0}", Server));
                //Sender Id - From firebase project setting  
                tRequest.Headers.Add(string.Format("Sender: id={0}", Sender));
                tRequest.ContentType = "application/json";
                var payload = new Message
                {
                    to = token,
                    data = new MessageData()
                    {
                        body = message,
                        title = title,
                        objectID = ObjectID,
                        type = Type,
                        click_action = link,
                        icon = IconUrl
                    },
                    Data = data
                };
                //        var payload = new
                //        {
                //            to = token,
                //            priority = "high",
                //            content_available = true,
                //            data = new
                //            {
                //                body = message,
                //                title = title,
                //                objectID = ObjectID,
                //                type = Type,
                //                click_action = link,
                //                icon = IconUrl
                //            },
                //            Data = new Dictionary<string, string>
                //{
                //        { "link",link }
                //}
                //        };

                string postbody = JsonConvert.SerializeObject(payload).ToString();
                Byte[] byteArray = Encoding.UTF8.GetBytes(postbody);
                tRequest.ContentLength = byteArray.Length;
                using (Stream dataStream = tRequest.GetRequestStream())
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);
                    using (WebResponse tResponse = tRequest.GetResponse())
                    {
                        using (Stream dataStreamResponse = tResponse.GetResponseStream())
                        {
                            if (dataStreamResponse != null) using (StreamReader tReader = new StreamReader(dataStreamResponse))
                                {
                                    sResponseFromServer = tReader.ReadToEnd();
                                }
                        }
                    }
                }
                return sResponseFromServer;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        //public async Task<string[]> Send(string[] tokens, string title, string message, string Type = "Notify", string link = null, string ObjectID = null)
        //{
        //    string sResponseFromServer = "";
        //    var tasks = new List<Task<string>>();

        //    foreach (var token in tokens)
        //    {
        //        Task<string> task = new Task<string>(() =>
        //        {
        //            try
        //            {
        //                WebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");
        //                tRequest.Method = "post";
        //                //serverKey - Key from Firebase cloud messaging server  
        //                tRequest.Headers.Add(string.Format("Authorization: key={0}", Server));
        //                //Sender Id - From firebase project setting  
        //                tRequest.Headers.Add(string.Format("Sender: id={0}", Sender));
        //                tRequest.ContentType = "application/json";
        //                var payload = new Message
        //                {
        //                    to = token,
        //                    data = new MessageData()
        //                    {
        //                        body = message,
        //                        title = title,
        //                        objectID = ObjectID,
        //                        type = Type,
        //                        click_action = link,
        //                        icon = IconUrl
        //                    }
        //                };
        //    //        var payload = new
        //    //        {
        //    //            to = token,
        //    //            priority = "high",
        //    //            content_available = true,
        //    //            data = new
        //    //            {
        //    //                body = message,
        //    //                title = title,
        //    //                objectID = ObjectID,
        //    //                type = Type,
        //    //                click_action = link,
        //    //                icon = IconUrl
        //    //            },
        //    //            Data = new Dictionary<string, string>
        //    //{
        //    //        { "link",link }
        //    //}
        //    //        };

        //                string postbody = JsonConvert.SerializeObject(payload).ToString();
        //                Byte[] byteArray = Encoding.UTF8.GetBytes(postbody);
        //                tRequest.ContentLength = byteArray.Length;
        //                using (Stream dataStream = tRequest.GetRequestStream())
        //                {
        //                    dataStream.Write(byteArray, 0, byteArray.Length);
        //                    using (WebResponse tResponse = tRequest.GetResponse())
        //                    {
        //                        using (Stream dataStreamResponse = tResponse.GetResponseStream())
        //                        {
        //                            if (dataStreamResponse != null) using (StreamReader tReader = new StreamReader(dataStreamResponse))
        //                                {
        //                                    sResponseFromServer = tReader.ReadToEnd();
        //                                }
        //                        }
        //                    }
        //                }
        //                return sResponseFromServer;
        //            }
        //            catch (Exception ex)
        //            {
        //                return ex.Message;
        //            }
        //        });
        //        tasks.Add(task);
        //        task.RunSynchronously();
        //    }
        //    return await Task.WhenAll(tasks);

        //}


    }
}
