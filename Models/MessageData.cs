﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FirebaseMessages
{
    public class MessageData
    {
        /// <summary>
        /// message type
        /// </summary>
        public string body { get; set; }
        /// <summary>
        /// message title
        /// </summary>
        public string title { get; set; }

        /// <summary>
        /// if you want to send an object with message to use in programing 
        /// </summary>
        public string objectID { get; set; }
        /// <summary>
        /// you can use it for make custome type , you can use it as commands to client to execute some of code
        /// </summary>
        public string type { get; set; } = "Notify";
        /// <summary>
        /// redirect user on click on notification 
        /// </summary>
        public string click_action { get; set; }
        /// <summary>
        /// must be online icon
        /// </summary>
        public string icon { get; set; }
    }
}
