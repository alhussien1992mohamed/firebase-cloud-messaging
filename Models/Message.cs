﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FirebaseMessages
{
    public class Message
    {
        public string to { get; set; }
        public string priority { get; set; } = "high";
        public bool content_available = true;
        public MessageData data { get; set; }
        public Dictionary<string, string> Data { get; set; }
    }
}
